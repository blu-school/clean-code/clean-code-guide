## Simple Object.assign() example
const course = {
    name: 'Web Programming',
    score: 75
};

const grade = {
    score: 92
};

// const copy = Object.assign({}, course);
// console.log(copy);

const finalResult = Object.assign(course, grade, { teacher: 'Mrs Water' });
//console.log(finalResult);


## Simple spread example
const user = {
    name: "Dom",
    age: 30,
    score: [80, 90]
};

const copiedWithSpread = { ...user, age: 60, occupation: "Web Developer" };
const copiedWithObjectAssign = Object.assign({}, user, {
    age: 60,
    occupation: "Web Developer"
});

// console.log(copiedWithSpread);
// console.log(copiedWithObjectAssign)

copiedWithSpread.score.push(100);
copiedWithObjectAssign.score.push(100);

// console.log("user",user); // because of shallow copy both objs are pointing to exact same array
// console.log("copiedWithSpread", copiedWithSpread);
// console.log("copiedWithObjectAssign", copiedWithObjectAssign);

## Undestanding shallow copy

/*For deep cloning, we need to use alternatives, because Object.assign() copies property values.

If the source value is a reference to an object, it only copies the reference value.*/

function test() {

    let obj1 = { a: 0, b: { c: 0 } };
    let obj2 = Object.assign({}, obj1);
    //console.log(JSON.stringify(obj2)); // { "a": 0, "b": { "c": 0}}

    // obj1.a = 1;
    // console.log(JSON.stringify(obj1)); // { "a": 1, "b": { "c": 0}}
    // console.log(JSON.stringify(obj2)); // { "a": 0, "b": { "c": 0}}

    // obj1.a = 2;
    // console.log(JSON.stringify(obj1)); // { "a": 1, "b": { "c": 0}}
    // console.log(JSON.stringify(obj2)); // { "a": 2, "b": { "c": 0}}

    // obj2.b.c = 3;
    // console.log(JSON.stringify(obj1)); // { "a": 1, "b": { "c": 3}}
    // console.log(JSON.stringify(obj2)); // { "a": 2, "b": { "c": 3}}

    // // Deep Clone
    // obj1 = { a: 0, b: { c: 0 } };
    // let obj3 = JSON.parse(JSON.stringify(obj1));
    // obj1.a = 4;
    // obj1.b.c = 4;
    // console.log(JSON.stringify(obj3)); // { "a": 0, "b": { "c": 0}}
}

test();

## Default object
function printName(options) {
    const defaults = {
        firstName: "Steve",
        lastName: "Jobs"
    }

    options = Object.assign(defaults, options);
   // console.log(`${options.firstName} ${options.lastName}`)
};


printName({
    firstName: "Dom"
})

## Difference between Object.assign() and spread operator

Object.defineProperty(Object.prototype, 'myProp', {
    set: () => console.log('Setter called')
  });
  
  const obj = { myProp: 42 };
  
  //Object.assign({}, obj); // Prints "Setter called"
  
  const newObj = { ...obj }; // Does **not** print "Setter called"



## Encapsulate conditionals
// Bad 

// not clear what are we checking if we are not familiar with the code base in details:
 
if (fsm.state === "fetching" && isEmpty(listNode)) {
    // ...
}


// Good / more readable:
function shouldShowSpinner(fsm, listNode) {
    return fsm.state === "fetching" && isEmpty(listNode);
}

if (shouldShowSpinner(fsmInstance, listNodeInstance)) {
    // ...
}

## Avoid negative conditionals

// Bad 
// When the logic is getting more complex (nested conditionals) it is getting also harder to follow.
// For the rare cases when we need only the negative option we can simply rename the function itself - isDOMNodeMissing


function isDOMNodeNotPresent(node) {
    // ...
}

if (!isDOMNodeNotPresent(node)) {
    // ...
}

//Good:

function isDOMNodePresent(node) {
    // do smth...
}

if (isDOMNodePresent(node)) {
    // do smth...
}


## Avoid conditionals 

//Bad:

function logMessage(message = "CRITICAL::The system …") {
    const parts = message.split("::");
    const level = parts[0];
    switch (level) {
        case 'NOTICE':
            console.log("Notice")
            break;
        case 'CRITICAL':
            console.log("Critical");
            break;
        case 'CATASTROPHE':
            console.log("Castastrophe");
            break;
    }
}

//Good:  Strategy pattern

const strategies = {
    criticalStrategy,
    noticeStrategy,
    catastropheStrategy,
}
function logMessage(message = "CRITICAL::The system …") {
    const [level, messageLog] = message.split("::");
    const strategy = `${level.toLowerCase()}Strategy`;
    const output = strategies[strategy](messageLog);
    return output;
}
function criticalStrategy(param) {
    console.log("Critical: " + param);
}
function noticeStrategy(param) {
    console.log("Notice: " + param);
}
function catastropheStrategy(param) {
    console.log("Catastrophe: " + param);
}
logMessage();
logMessage("CATASTROPHE:: A big Catastrophe");
logMessage("NOTICE::Take some notice");

// Strategy Pattern: Basic Idea
// The strategy pattern is a behavioral design pattern that enables selecting an algorithm at runtime.
// — Wikipedia
// Define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from clients that use it.
// - Design Patterns: Elements of Reusable Object-Oriented Software

## Functions should only be one level of abstraction

Bad (multiple levels of abstractions in one function):

const placeOrder = ({ order }) => {
	validateAvailability(order);
	
	const total = order.items.reduce(
		(item, totalAcc) =>
		totalAcc + item.unitPrice * item.units,
		0,
	);
	const invoiceInfo = getInvoiceInfo(order);
	const request = new PaymentService.Request({
		total,
		invoiceInfo,
	});
	const response = PaymentService.pay(request);
	
	sendInvoice(response.invoice);
	
	shipOrder(order);
};

Good (several functions with one level of abstraction):

const getTotal = order =>
  order.items.reduce(
    (item, totalAcc) =>
      totalAcc + item.unitPrice * item.units,
    0,
  );

const pay = (total, invoiceInfo) => {
  const request = new PaymentService.Request({
    total,
    invoiceInfo,
  });
  const response = PaymentService.pay(request);

  sendInvoice(response.invoice);
};

const payOrder = order => {
  const total = getTotal(order);
  const invoiceInfo = getInvoiceInfo(order);
  
  pay(total, invoiceInfo);
};

const placeOrder = ({ order }) => {
  validateAvailability(order);
  payOrder(order);
  shipOrder(order);
};

